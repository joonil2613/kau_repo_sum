#ifndef Att_TestFcn

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include "Type_Wheel.h"
#include <unistd.h> 
#include <stdlib.h>
#include <fcntl.h> 
#include <termios.h> 
#include <errno.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#define ROLL 1
#define PITCH 2
#define YAW 3

#define EulerAngle 53
#define GyroRate 52

#define CJMCU_ID 74
#define LightSetting 195
#define LIGHTCONFIG 2
#define HIGHBYTE 3
#define LOWBYTE 4

float bytes2float(unsigned char* bytes_array);
char Checksum(char *msg, int len);
int SetDevice(char* device);
float GetValue(int fd, int input, int com);
void GetCommand(int cnt, int com);

int wheel_fd;
int SetWheelSpeed(int fd, float Speed);
int SetWheelTorque(int fd, float Torque);
void float2bytes(float data,unsigned char* bytes_array);
int StopWheel(int fd);
int GetTelemetryStandard(int fd, telemetry_t *tlm_set);

int exp_two(int b);
void GetLux(int value, int LowHigh);
int Exponent, Mantissa;

#endif
