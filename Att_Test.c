#include "Att_TestFcn.h"
//#include "Type_Wheel.h"


int main(int argc, char *argv[])
{
	int count_arg, cnt;
	float command;
	int Loopnumber = 0;

	time_t Current_time;
	struct tm TM;
	struct timeval TM_milsec;

	float Roll, Pitch, Yaw;
	float Roll_rate, Pitch_rate, Yaw_rate;

	float Lux;
	int LuxHighbyte, LuxLowbyte;
	int Light_ID = wiringPiI2CSetup(CJMCU_ID);

	char Datafile[256];

	wiringPiI2CWriteReg8(Light_ID, LIGHTCONFIG, LightSetting);
//	printf( "\nCommand-line arguments:\n" );
//	    for( count_arg = 0; count_arg < argc; count_arg++ )
//		printf( "  argv[%d]   %s\n", count_arg, argv[count_arg] );

	printf("How many get data?\r\n");
	scanf("%d", &cnt);

//	printf("Wheel Init\r\n");
	int fd = SetDevice("/dev/ttyUSB0");

//	command = atof(argv[1]);
//	telemetry_t *tlm_set;
//	printf("command : %f",command);
//	SetWheelSpeed(fd,command);
	
	Current_time = time(NULL);
	TM = *localtime(&Current_time);

	sprintf(Datafile, "%d-%02d-%02d_%02d:%02d:%02d.txt", TM.tm_year+1900, TM.tm_mon+1, TM.tm_mday, TM.tm_hour, TM.tm_min, TM.tm_sec);
	char *Datafilename = Datafile;
	FILE *Newfile = fopen(Datafilename, "w");

	printf("-------------------------------------\r\n\n");
	printf("Time\tRoll\t Pitch\t Yaw");
	printf("\t Roll_rate\t Pitch_rate\t Yaw_rate\t Lux \r\n\n");
	printf("-------------------------------------\r\n\n");
	
	while(Loopnumber < cnt)
	{

		Roll = GetValue(fd, ROLL, EulerAngle);
		Pitch = GetValue(fd, PITCH, EulerAngle);
		Yaw = GetValue(fd, YAW, EulerAngle);
		Roll_rate = GetValue(fd, ROLL, GyroRate);
		Pitch_rate = GetValue(fd, PITCH, GyroRate);
		Yaw_rate = GetValue(fd, YAW, GyroRate);

		LuxHighbyte = wiringPiI2CReadReg8(Light_ID, HIGHBYTE);
		LuxLowbyte = wiringPiI2CReadReg8(Light_ID, LOWBYTE);
		GetLux(LuxHighbyte, HIGHBYTE);
		GetLux(LuxLowbyte, LOWBYTE);
		Lux = exp_two(Exponent) * Mantissa * 0.045;

		Current_time = time(NULL);
		TM = *localtime(&Current_time);
		gettimeofday(&TM_milsec, NULL);

		printf("%02d:%02d:%02d.%02d", TM.tm_hour, TM.tm_min, TM.tm_sec, TM_milsec.tv_usec / 10000);
		printf("\t%0.2f\t%0.2f\t%0.2f", Roll, Pitch, Yaw);
		printf("\t%0.2f\t%0.2f\t%0.2f\t%0.2f \r\n", Roll_rate, Pitch_rate, Yaw_rate, Lux);

		fprintf(Newfile, "%02d:%02d:%02d.%02d", TM.tm_hour, TM.tm_min, TM.tm_sec, TM_milsec.tv_usec / 10000);
		fprintf(Newfile, "\t%0.2f\t%0.2f\t%0.2f", Roll, Pitch, Yaw);
		fprintf(Newfile, "\t%0.2f\t%0.2f\t%0.2f\t%0.2f \r\n", Roll_rate, Pitch_rate, Yaw_rate, Lux);

		Loopnumber++;

	}

	close(fd);
	fclose(Newfile);
	//GetTelemetryStandard(fd,tlm_set);
}
