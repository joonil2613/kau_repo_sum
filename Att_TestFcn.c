#include "Att_TestFcn.h"

#define DEBUG

unsigned char ReadDataCommand[13] = {0x02, 0x0D, 0x01, 0x3C, 0x00,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x00, 0x03};



float bytes2float(unsigned char* bytes_array)
{
	
  union {
    float float_variable;
    char temp[4];
  } u;
  
  for (int k = 0; k < 4; k++)
  {
	  u.temp[k] = bytes_array[k];
  }
  
  return u.float_variable;
}

char Checksum(char *msg, int len)
{
	char cs = 0;
	for (int j = 0; j < len; j++)
	{
		cs += msg[j + 3];
	}
	return cs;
}

void GetCommand(int cnt, int com)
{
	if (cnt == ROLL)
	{
		ReadDataCommand[6] = ROLL;
	}
	else if (cnt == PITCH)
	{
		ReadDataCommand[6] = PITCH;
	}
	else if (cnt == YAW)
	{
		ReadDataCommand[6] = YAW;
	}
	ReadDataCommand[4] = com;
	ReadDataCommand[11] = Checksum(ReadDataCommand, 8);
}

float GetValue(int fd, int input, int com)
{
 	unsigned char buf[13];
	int i;
	unsigned char buf2float[4];
	float Value = 0;

	GetCommand(input, com);
	
	int count = write(fd, ReadDataCommand, 13);
	count = read(fd, buf, 13);
	
	for (i = 0; i < 4; i++)
	{
		buf2float[i] = buf[i + 7];
	}
	
	Value = bytes2float(buf2float);
	return Value;
}

void float2bytes(float data,unsigned char* bytes_array)
{
  // Create union of shared memory space
  union {
    float float_variable;
    unsigned char temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.float_variable = data;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}
int SetWheelSpeed(int fd, float Speed)
{
	int bytes=0;
 	unsigned char buf[255];
	unsigned char fbuf[4];
	
	int i=0;
	unsigned char command[6];

	float2bytes(Speed,fbuf);
	command[0] = 0xD2;
	command[5] = 0xAC;

	for(i=0;i<4;i++)
	{
		command[1+i] = fbuf[3-i];	
	}
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Send data = %x\r\n",command[i]);
	}
	#endif
	int count = write(fd, command, 6);
	//count = read(fd,&buf, 6);
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		count = read(fd,&buf,1);//printf("Count : %d\r\n", count);
		printf("Rcv data = %x\r\n",buf[0]);
	}
	#endif
	


}

int StopWheel(int fd)
{
	int bytes=0;
 	unsigned char buf[6];
	int i=0;
	unsigned char command[6];
	command[0] = 0xD2;
	command[5] = 0xAC;

	for(i=0;i<4;i++)
	{
		command[1+i] = 0;	
	}
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Send data = %x\r\n",command[i]);
	}
	#endif
	int count = write(fd, command, 6);
 	count = read(fd, buf, 6);
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Rcv data = %x\r\n",buf[i]);
	}
	#endif


}

int SetWheelTorque(int fd, float Torque)
{
	int bytes=0;
 	unsigned char buf[6];
	int i=0;
	unsigned char command[6];
	unsigned char fbuf[4];

	float2bytes(Torque,fbuf);
	command[0] = 0xD3;
	command[5] = 0xAC;
	for(i=0;i<4;i++)
	{
		command[1+i] = fbuf[3-i];	
	}
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Send data[%d] = %x\r\n",i,command[i]);
	}
	#endif
	int count = write(fd, command, 6);
	count = read(fd, buf, 6);
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Rcv data = %x\r\n",buf[i]);
	}
	#endif

		

}

int GetTelemetryStandard(int fd, telemetry_t *tlm_set)
{
	int bytes=0;
 	unsigned char buf[29];
	int i=0;
	unsigned char command[6];
	unsigned char fbuf[4];
	int count;
	
	command[0] = 0xDD;
	command[5] = 0xAC;
	for(i=0;i<4;i++)
	{
		command[1+i] = 0;	
	}
	#ifdef DEBUG
	for(i=0;i<6;i++)
	{
		printf("Send data[%d] = %x\r\n",i,command[i]);
	}
	#endif
	count = read(fd, buf, 29);
#ifdef DEBUG
	for(i=0;i<29;i++)
	{
		printf("Rcv data[%d] = %x\r\n",i,buf[i]);
	}
#endif
	tlm_set->serial_no = buf[0];
	tlm_set->status_register[0] = buf[1];
	tlm_set->status_register[1] = buf[2];
	
	for(i=0;i<4;i++)
	{
		tlm_set->target_value[i] = buf[3+i];
		tlm_set->actual_speed_c[i] = buf[7+i];
		tlm_set->temp_c[i] = buf[15+i];
	};


}

int exp_two(int b)
{
	int a = 1;
	while(b > 0)
	{
		a = a * 2;
		b--;
	}
	return a;
}

void GetLux(int value, int LowHigh)
{
	if(LowHigh == 3)
	{
		Exponent = 0;
		Mantissa = 0;
		for (int i = 7; i >= 4; i--)
		{
			Exponent += exp_two(i - 4) * ((value >> i) & 1);
		}

		for (int j = 3; j >= 0; j--)
		{
			Mantissa += exp_two(4 + j) * ((value >> j) & 1);
		}
	}

	else if(LowHigh == 4)
	{
		for (int i = 3; i >= 0; i--)
		{
			Mantissa += exp_two(i) * ((value >> i) & 1);
		}
	}
}

int SetDevice(char *device)
{
	int sfd = open(device, O_RDWR | O_NOCTTY ); 
	if (sfd == -1){
	  printf ("Error no is : %d\n", errno);
	  printf("Error description is : %s\n",strerror(errno));
	  return(-1);
	}
	struct termios options;
	tcgetattr(sfd, &options);
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;		//<Set baud rate
//	options.c_cflag &= ~ PARENB;
//	options.c_cflag &= ~CSTOPB;
//	options.c_cflag &= ~CSIZE;
//	options.c_cflag &= ~CRTSCTS;
	cfsetispeed(&options,B115200);
	cfsetospeed(&options,B115200);
	options.c_iflag = IGNPAR;
//	options.c_iflag &= ~(IXON|IXOFF|IXANY);
//	options.c_iflag &= ~(ICANON|ECHO|ECHOE|ISIG);
	options.c_oflag = 0;
	options.c_lflag = 0;//ICANON;
//	options.c_cc[VMIN] = 1;
//	options.c_cc[VTIME] = 1;	
	tcflush(sfd, TCIFLUSH);
	if(tcsetattr(sfd, TCSANOW, &options)!=0)
		printf("Error\r\n");
	//set_blocking(fd,0)
	return sfd;
}
